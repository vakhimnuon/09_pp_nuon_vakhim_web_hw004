import React from "react";

function Form(props) {
  return (
    <form onSubmit={props.onSubmit}>
      <label htmlFor="input1">Input 1:</label>
      <input
        type="text"
        id="input1"
        name="input1"
        value={props.input1}
        onChange={props.onInputChange}
      />

      <label htmlFor="input2">Input 2:</label>
      <input
        type="text"
        id="input2"
        name="input2"
        value={props.input2}
        onChange={props.onInputChange}
      />

      <label htmlFor="input3">Input 3:</label>
      <input
        type="text"
        id="input3"
        name="input3"
        value={props.input3}
        onChange={props.onInputChange}
      />

      <button type="submit">Submit</button>
    </form>
  );
}

export default Form;