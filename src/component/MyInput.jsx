import React, { Component } from "react";
import MyTable from "./MyTable";
import "animate.css";
import Swal from "sweetalert2";

export default class MyInput extends Component {
  constructor() {
    super();
    this.state = {
      person: [
        {
          id: 1,
          email:"Vakhimnnuon@gmail.com",
          username: "Vakhim",
          age: 20,
          status: "pending",
        },
      ],
      newEmail: "",
      newUname: "",
      newAge: "",
      newStatus: "Pending",
    };
  }
//sweet alert
  onAlert = (person) => {
    Swal.fire({
      title: `ID : ${person.id} <br/> Email: ${person.email} <br/> Username: ${person.username} <br/> Age: ${person.age}`,
      showClass: {
        popup: "animate__animated animate__fadeInDown",
      },
      hideClass: {
        popup: "animate__animated animate__fadeOutUp",
      },
    });
  };

  handleGmail = (event) => {
    this.setState({
      newEmail: event.target.value,
    });
  };
  handleUserName = (event) => {
    this.setState({
      newUname: event.target.value,
    });
  };
  handleAge = (event) => {
    this.setState({
      newAge: event.target.value,
    });
  };

  //Insert 
  handleClick = () => {
    const newObj = {
      id: this.state.person.length + 1,
      email: this.state.newEmail,
      username: this.state.newUname,
      age: this.state.newAge,
      status: this.setState({status: 'Done'})
    };

    const spreadmyObj = { ...newObj };

    this.setState(
      {
        person: [...this.state.person, spreadmyObj],
        newEmail: "",
        newUname: "",
        newAge: "",
      },
      () => console.log(this.state.person)
    );
  };
  

  render() {
    return (
      <div className="bg-amber-100 p-5  pb-96">
        <h1 className=" text-3xl font-bold text-purple-600">Please Fill Your information</h1>
        <div className="container flex justify-center item-center m-auto">
          <div className="form-control w-[60%] ">
            <label className="label ">
              <span className="label-text text-lg ">Your Email</span>
            </label>
            <label className="input-group ">
              <span>📧</span>
              <input
                type="text"
                placeholder="name@gmail.com"
                className="w-full input input-bordered items-center"
                onChange={this.handleGmail}
              />
            </label>
          </div>
        </div>
        <div className=" container flex  justify-center mx-auto">
          <div className="form-control w-[60%] ">
            <label className="label">
              <span className="label-text text-lg ">Username</span>
            </label>
            <label className="input-group">
              <span>@</span>
              <input
                type="text"
                placeholder="username"
                className="input w-full input-bordered"
                onChange={this.handleUserName}
              />
            </label>
          </div>
        </div>
        <div className=" container flex justify-center m-auto">
          <div className="form-control w-[60%]">
            <label className="label">
              <span className="label-text text-lg ">Age</span>
            </label>
            <label className="input-group">
              <span>❤</span>
              <input
                type="text"
                placeholder="age"
                className="w-full input input-bordered"
                onChange={this.handleAge}
              />
            </label>
          </div>
        </div>
        <button
          onClick={this.handleClick}
          className="btn btn-outline btn-primary m-5 w-56 bg-yellow-50"
        >
          Register
        </button>
        <MyTable getData={this.state.person} sweetMyAlert={this.onAlert} />
      </div>
    );
  }
}
