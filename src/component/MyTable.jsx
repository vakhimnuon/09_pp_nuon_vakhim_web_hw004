import React, { Component } from "react";

export default class MyTable extends Component {
  render() {
    return (
      <div>
        <div class=" w-[70%] m-auto">
          <table class="w-full text-sm text-center bg-black text-gray-500 ">
            <thead class="text-lg text-white uppercase  dark:bg-gray-700 ">
              <tr>
                <th scope="col" class="px-6 py-3">
                  ID
                </th>
                <th scope="col" class="px-6 py-3">
                  Gmail
                </th>
                <th scope="col" class="px-6 py-3">
                  Username
                </th>
                <th scope="col" class="px-6 py-3">
                  Age
                </th>
                <th scope="col" class="px-6 py-3">
                  Status
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.getData.map((item) => (
                <tr
                  key={item.id}
                  className="border-2 bg-blue-300 text-lg">
                  <td class="px-6 py-4">{item.id} </td>
                  <td class="px-6 py-4">{item.email=== ''?"null":item.email} </td>
                  <td class="px-6 py-4">{item.username===''?"null":item.username} </td>
                  <td class="px-6 py-4">{item.age===''?"null":item.age} </td>
                  <td class="px-6 py-4">
                    <button
                      type="button" className="btn btn-error m-5" >{item.status}
                    </button>
                    <button
                      onClick = {() => {this.props.sweetMyAlert(item)}}
                      type="button"
                      className="btn btn-success bg-primary-focus">
                      Show More
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
